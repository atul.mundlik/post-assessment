package com.xnsio.cleancode;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

public class SchedulingAssistantTest {

	private final Set<String> attendeesMiniAndFrank = new HashSet<>(Arrays.asList("Mini", "Frank"));
	private final LocalDateTime TOMORROW_11_AM = LocalDate.now().plusDays(1).atTime(11, 0);
	private final LocalDateTime TOMORROW_12_AM = LocalDate.now().plusDays(1).atTime(12, 0);
	private SchedulingAssistant meetingSchedule;
	private final Set<String> attendeesNickAndJanet = new HashSet<>(Arrays.asList("Nick", "Janet"));
	private final Set<String> attendeesMiniAndBrad = new HashSet<>(Arrays.asList("Mini", "Brad"));;

	@Before
	public void setUp() {
		meetingSchedule = new SchedulingAssistant(8, 16);
	}

	@Test
	public void scheduleMeetingInFreeBusinessHours() throws Exception {
		assertEquals(successMessageWith(upcomingBusinessHour(), attendeesMiniAndFrank),
				meetingSchedule.schedule(attendeesMiniAndFrank, TOMORROW_11_AM));
	}

	@Test
	public void scheduleMeetingInEarliestAvailableHours() throws Exception {
		scheduleMeetingsUpto(TOMORROW_11_AM);
		LocalDateTime tommorow1Pm = LocalDate.now().plusDays(1).atTime(13, 0);
		assertEquals(successMessageWith(TOMORROW_11_AM, attendeesMiniAndFrank), meetingSchedule.schedule(attendeesMiniAndFrank, tommorow1Pm));
	}

	@Test
	public void dontScheduleIfTimeNotAvailable() throws Exception {
		scheduleMeetingsUpto(TOMORROW_12_AM);
		assertEquals(SchedulingAssistant.NO_SLOT_AVAILABLE + attendeesMiniAndFrank.stream().collect(Collectors.joining(",")), 
				meetingSchedule.schedule(attendeesMiniAndFrank, TOMORROW_11_AM));
	}

	@Test
	public void scheduleForFreePersonsOnly() throws Exception {
		scheduleMeetingsUpto(TOMORROW_11_AM);
		assertEquals(successMessageWith(upcomingBusinessHour(), attendeesNickAndJanet),
				meetingSchedule.schedule(attendeesNickAndJanet, TOMORROW_12_AM));
		assertEquals(successMessageWith(TOMORROW_11_AM, attendeesMiniAndBrad), meetingSchedule.schedule(attendeesMiniAndBrad, TOMORROW_12_AM));
	}

	private String successMessageWith(LocalDateTime upcomingBusinessHour, Set<String> attendees) {
		return 
				SchedulingAssistant.DATE_PATTERN.format(upcomingBusinessHour) + SchedulingAssistant.MINUTE_SUFFIX + 
				SchedulingAssistant.SCHEDULED_SUCCESSFULLY + attendees.stream().collect(Collectors.joining(","));
	}

	@Test
	public void dontScheduleExceptBusinessHours() throws Exception {
		LocalDateTime tommorow5Pm = LocalDate.now().plusDays(1).atTime(17, 0);
		scheduleMeetingsUpto(tommorow5Pm);
		LocalDateTime tommorow6Pm = LocalDate.now().plusDays(1).atTime(18, 0);
		assertEquals(SchedulingAssistant.NO_SLOT_AVAILABLE + attendeesMiniAndFrank.stream().collect(Collectors.joining(",")),
				meetingSchedule.schedule(attendeesMiniAndFrank, tommorow6Pm));
	}

	private void scheduleMeetingsUpto(LocalDateTime endTime) {
		LocalDateTime now = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0);
		while (!now.isAfter(endTime)) {
			meetingSchedule.schedule(attendeesMiniAndFrank, now);
			now = now.plusHours(1);
		}
	}

	private LocalDateTime upcomingBusinessHour() {
		LocalDateTime now = LocalDateTime.now();
		if (now.getHour() > 17 || now.getHour() < 8) {
			now = now.plusDays(1).withHour(8);
		}
		return now;
	}

}

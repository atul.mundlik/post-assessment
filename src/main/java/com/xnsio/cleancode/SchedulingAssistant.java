package com.xnsio.cleancode;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class SchedulingAssistant {

	public static final String MINUTE_SUFFIX = ":00";
	public static final DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ISO_DATE;
	public static final String NO_SLOT_AVAILABLE = "No available time-slot for ";
	public static final String SCHEDULED_SUCCESSFULLY = ", is the first available time-slot for ";
	private final HashMap<String, Set<LocalDateTime>> employeeCalendar = new HashMap<>();
	
	private int dayStartHour = 8;
	private int dayEndHour = 5;
	
	SchedulingAssistant(int dayStartHour, int dayEndHour) {
		this.dayStartHour = dayStartHour;
		this.dayEndHour = dayEndHour;
	}

	public String schedule(Set<String> attendees, LocalDateTime endTime) {
		LocalDateTime availableHour = firstAvailableHour(attendees);
		if (availableHour.isAfter(endTime)) {
			return NO_SLOT_AVAILABLE + attendees.stream().collect(Collectors.joining(","));
		}
		endTime = availableHour;
		for (String attendee : attendees) {
			Set<LocalDateTime> busyHours = employeeCalendar.get(attendee);
			if (busyHours == null) {
				busyHours = new HashSet<>();
			}
			busyHours.add(endTime = endTime.withMinute(0).withSecond(0).withNano(0));
			employeeCalendar.put(attendee, busyHours);
		}
		return DATE_PATTERN.format(endTime) + MINUTE_SUFFIX + SCHEDULED_SUCCESSFULLY
				+ attendees.stream().collect(Collectors.joining(","));
	}
	
	private Set<LocalDateTime> busyHours(Set<String> attendees) {
		Set<LocalDateTime> attendeesBusyHours = new LinkedHashSet<>();
		for (String attendee : attendees) {
			if (null != employeeCalendar.get(attendee))
				attendeesBusyHours.addAll(employeeCalendar.get(attendee));
		}
		return attendeesBusyHours;
	}

	private LocalDateTime firstAvailableHour(Set<String> attendees) {
		Set<LocalDateTime> attendeesBusyHours = busyHours(attendees);
		LocalDateTime min = LocalDateTime.now();
		min = skipNonBusinessHours(min);
		if (!attendeesBusyHours.isEmpty()) {
			min = Arrays.asList(min, Collections.min(attendeesBusyHours)).stream().sorted()
					.max(LocalDateTime::compareTo).get();
			LocalDateTime max = Collections.max(attendeesBusyHours).withMinute(1);
			min = min.withMinute(0).withSecond(0).withNano(0);
			while (!min.isAfter(max)) {
				if (!attendeesBusyHours.contains(min)) {
					break;
				}
				min = min.plusHours(1);
				min = skipNonBusinessHours(min);
			}
		}
		return min;
	}

	private LocalDateTime skipNonBusinessHours(LocalDateTime min) {
		if (min.getHour() > dayEndHour || min.getHour() < dayStartHour) {
			min = min.plusDays(1).withHour(dayStartHour);
		}
		return min;
	}
}
